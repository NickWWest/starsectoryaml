package data.scripts.rules;

import java.util.ArrayList;
import java.util.List;

public class RuleFactory {

    public static List<RuleSet> buildRuleSets(List<RawRule> rawRules){
        if(rawRules.size() < 1 || !rawRules.get(0).isComment()){
            throw new IllegalArgumentException("rawRules must be valid");
        }

        List<RuleSet> ret = new ArrayList<>();

        RuleSet current = new RuleSet();
        for(RawRule rawRule : rawRules){
            if(rawRule.isEmpty()){
                continue;
            }

            if(rawRule.isComment()){ // found a comment, start a new ruleset

                current = new RuleSet();
                ret.add(current);
                current.comment = rawRule.getComment();
                continue;
            }

            current.rules.add(new Rule(rawRule));
        }

        return ret;
    }

    public static List<RawRule> buildRawRules(List<RuleSet> ruleSets){

        List<RawRule> ret = new ArrayList<>();
        for(RuleSet rs : ruleSets){
            ret.add(new RawRule(rs.comment));
            for(Rule r : rs.rules){
                ret.add(new RawRule(r));
            }
        }

        return ret;
    }

}
