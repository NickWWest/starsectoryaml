package data.scripts;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import com.univocity.parsers.common.RowProcessorErrorHandler;
import com.univocity.parsers.csv.CsvParserSettings;
import com.univocity.parsers.csv.CsvRoutines;
import com.univocity.parsers.csv.CsvWriterSettings;
import data.scripts.rules.RawRule;
import data.scripts.rules.RuleFactory;
import data.scripts.rules.RuleSet;
import picocli.CommandLine;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


//Example args: --action ToYaml --csv "D:\Games\Starsector8.1a\mods\Steiner Foundation\data\campaign\rules.csv" --yaml "D:\Games\Starsector8.1a\mods\Steiner Foundation\data\campaign\rules.yaml"

@CommandLine.Command(description = "Reads in a rules.csv file and outputs a rules.yaml file",  name = "CsvToYaml", mixinStandardHelpOptions = true)
public class RulesParser implements Runnable {

    public enum Action {
        ToYaml,
        ToCsv
    }

    public static void main(String... args) {
        CommandLine.run(new RulesParser(), System.err, args);
    }

    @CommandLine.Option(names = {"-c", "--csv"}, description = "The path of the csv rules file")
    public File csvFile;

    @CommandLine.Option(names = {"-y", "--yaml"}, description = "The path of the YAML rules file")
    public File yamlFile;

    @CommandLine.Option(names = {"-a", "--action"}, description = "Valid actions are: 'ToYaml' & 'ToCsv'")
    public Action action;

    @Override
    public void run() {
        try {

            switch (action) {
                case ToYaml:
                    parseCsvAndSaveToYaml();
                    break;
                case ToCsv:
                    parseYamlAndSaveToCsv();
                    break;
                default:
                    throw new RuntimeException("Unknown action type: " + action);
            }

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void parseCsvAndSaveToYaml() throws Exception {

        System.out.println("Converting CSV to YAML");
        try(Reader reader = new BufferedReader(new FileReader(csvFile))) {
            List<RawRule> rawRules = parseCsv(reader);
            System.out.println(rawRules.size() + " raw rules found");

            List<RuleSet> ruleSets = RuleFactory.buildRuleSets(rawRules);
            System.out.println(ruleSets.size() + " rulesets built");

            if(yamlFile.exists()){
                yamlFile.delete();
            }
            try(OutputStream os = new BufferedOutputStream(new FileOutputStream(yamlFile))) {
                writeYAML(ruleSets, os);
            }

            System.out.println("Finished writing output to: '"+yamlFile+"'");
        }
    }

    private void parseYamlAndSaveToCsv() throws Exception {
        System.out.println("Converting YAML to CSV");
        try(Reader reader = new BufferedReader(new FileReader(yamlFile))) {
            List<RuleSet> ruleSets = parseYaml(reader);
            System.out.println(ruleSets.size() + " RuleSets found");
            try(OutputStream os = new BufferedOutputStream(new FileOutputStream(csvFile))) {
                writeCsv(ruleSets, os);
            }

            System.out.println("Finished writing output to: '"+csvFile+"'");
        }
    }


    public static void writeYAML(List<RuleSet> ruleSets, OutputStream outStream) throws IOException {
        YAMLFactory factory = new YAMLFactory();
        factory.enable(YAMLGenerator.Feature.MINIMIZE_QUOTES) //removes quotes from strings
                .disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER)//gets rid of -- at the start of the file.
                .enable(YAMLGenerator.Feature.INDENT_ARRAYS);// enables indentation.

        ObjectMapper mapper = new ObjectMapper(factory);
        SequenceWriter sw = mapper.writerWithDefaultPrettyPrinter().writeValues(outStream);
        sw.write(ruleSets);
    }

    public static List<RawRule> parseCsv(Reader ruleCsv){
        CsvParserSettings settings = new CsvParserSettings();
        settings.getFormat().setLineSeparator("\n");
        settings.getFormat().setDelimiter(',');
        settings.getFormat().setQuote('\"');
        settings.getFormat().setQuoteEscape('\"');
        settings.setSkipEmptyLines(true);
        settings.setHeaderExtractionEnabled(true);
        settings.getFormat().setComment('\0');

        settings.setProcessorErrorHandler((RowProcessorErrorHandler) (error, inputRow, context) -> {
            System.out.println("Error processing row: " + Arrays.toString(inputRow));
            System.out.println("Error details: column '" + error.getColumnName() + "' (index " + error.getColumnIndex() + ") has value '" + inputRow[error.getColumnIndex()] + "'");
        });

        CsvRoutines routines = new CsvRoutines(settings);
        ArrayList<RawRule> ret = new ArrayList<>();
        for(RawRule rawRule : routines.iterate(RawRule.class, ruleCsv)){
            if(!rawRule.isEmpty()) {
                ret.add(rawRule);
            }
        }

        return ret;
    }

    public static void writeCsv(List<RuleSet> ruleSets, OutputStream ruleCsv) {
        CsvWriterSettings settings = new CsvWriterSettings();
        settings.getFormat().setLineSeparator("\n");
        settings.getFormat().setDelimiter(',');
        settings.getFormat().setQuote('\"');
        settings.getFormat().setQuoteEscape('\"');
        settings.setQuoteEscapingEnabled(true);
        settings.setSkipEmptyLines(true);
        settings.getFormat().setComment('\0');
        settings.setHeaderWritingEnabled(true);

        settings.setProcessorErrorHandler((RowProcessorErrorHandler) (error, inputRow, context) -> {
            System.out.println("Error processing row: " + Arrays.toString(inputRow));
            System.out.println("Error details: column '" + error.getColumnName() + "' (index " + error.getColumnIndex() + ") has value '" + inputRow[error.getColumnIndex()] + "'");
        });

        List<RawRule> rawRules = RuleFactory.buildRawRules(ruleSets);
        System.out.println(rawRules.size() + " Raw Rules found");

        new CsvRoutines(settings).writeAll(rawRules, RawRule.class, ruleCsv);

    }
    public static List<RuleSet> parseYaml(Reader ruleYaml){
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {

            RuleSet[] ruleSets = mapper.readValue(ruleYaml, RuleSet[].class);
            return Arrays.stream(ruleSets).collect(Collectors.toList());
        } catch (Exception e) {


            throw new RuntimeException(e);
        }
    }
}
