package data.scripts;

import data.scripts.rules.RawRule;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringReader;
import java.util.List;

public class RawRuleCsvParserTest {

    @Test
    public void TestMultiLineRuleParsing(){
        String toParse = "a,b,c\n"+
        "1,2,\"\n3\"";
        List<RawRule> rawRules = RulesParser.parseCsv(new StringReader(toParse));
        Assert.assertEquals(1, rawRules.size());
        Assert.assertEquals("1", rawRules.get(0).id);
        Assert.assertEquals("2", rawRules.get(0).trigger);
        Assert.assertEquals("3", rawRules.get(0).conditions);
    }

    @Test
    public void TestParseComment(){
        String toParse =
                "id,trigger,conditions,script,text,options,notes\n"+
                "# default entity interaction,,,,,,\n";
        List<RawRule> rawRules = RulesParser.parseCsv(new StringReader(toParse));
        Assert.assertEquals(1, rawRules.size());
        Assert.assertEquals("# default entity interaction", rawRules.get(0).id);
    }

    @Test
    public void TestComplexRuleParsing1(){
        String multiLineRule =
                "id,trigger,conditions,script,text,options,notes\n"+
                        "# default entity interaction,,,,,,\n"+
                        "defaultOpenDialog,OpenInteractionDialog,,\"ShowDefaultVisual\n"+
                        "PrintDescription 3\n"+
                        "SetShortcut defaultLeave \"\"ESCAPE\"\"\",,defaultLeave:Leave,\n"+
                        "defaultLeave,DialogOptionSelected,$option == defaultLeave,DismissDialog,,,\n";

        List<RawRule> rawRules = RulesParser.parseCsv(new StringReader(multiLineRule));

        for(RawRule rawRule : rawRules){
            System.out.println(rawRule);
        }
    }

    @Test
    public void TestComplexRuleParsing2(){
        String multiLineRule =
                "id,trigger,conditions,script,text,options,notes\n" +
                "# warning beacons,,,,,,\n" +
                "beaconOpenDialog,OpenInteractionDialog,$tag:warning_beacon,\"ShowDefaultVisual\n" +
                "SetShortcut beaconLeave \"\"ESCAPE\"\"\",An autonomous warning beacon has been left emitting a distorted wail between official channels. Whatever message it was meant to convey has been corrupted.,beaconLeave:Leave,\n" +
                "beaconOpenDialogRemnantsDestroyed,OpenInteractionDialog,\"$tag:warning_beacon\n" +
                "$remnantDestroyed\",\"ShowDefaultVisual\n" +
                "SetShortcut beaconLeave \"\"ESCAPE\"\"\",\"This autonomous warning beacon emits a looping message.\n" +
                "\n" +
                "\"\"DANGER: This star system is known to contain potentially active autonomous weapon systems. Access to this system by unauthorized parties is forbidden by Hegemony Navy diktat 224.34.\"\"\",beaconLeave:Leave,\n" +
                "beaconOpenDialogRemnantsSuppressed,OpenInteractionDialog,\"$tag:warning_beacon\n" +
                "$remnantSuppressed\",\"ShowDefaultVisual\n" +
                "SetShortcut beaconLeave \"\"ESCAPE\"\"\",\"This autonomous warning beacon emits a looping message.\n" +
                "\n" +
                "\"\"DANGER: This star system is known to contain potentially active autonomous weapon systems. Access to this system by unauthorized parties is forbidden by Hegemony Navy diktat 224.34.\"\"\",beaconLeave:Leave,\n" +
                "beaconOpenDialogRemnantsResurgent,OpenInteractionDialog,\"$tag:warning_beacon\n" +
                "$remnantResurgent\",\"ShowDefaultVisual\n" +
                "SetShortcut beaconLeave \"\"ESCAPE\"\"\",\"This autonomous warning beacon emits a looping message.\n" +
                "\n" +
                "\"\"DANGER: This star system is known to contain potentially active autonomous weapon systems. Access to this system by unauthorized parties is forbidden by Hegemony Navy diktat 224.34.\"\"\",beaconLeave:Leave,\n" +
                "beaconOpenDialogDaedaleon,OpenInteractionDialog,\"$tag:warning_beacon\n" +
                "$daedaleon\n" +
                "\",\"ShowDefaultVisual\n" +
                "SetShortcut beaconLeave \"\"ESCAPE\"\"\",\"This autonomous warning beacon emits a looping message.\n" +
                "\n" +
                "\"\"This world is corrupted by technological sin and its abomination stands testament to the result of straying from the path of virtue. Trespass is forbidden; any who scorns this warning shall become both corrupt of body and outlaw by the holy justice enforced by this order of the Knights of Ludd.\"\"\",beaconLeave:Leave,\n" +
                "beaconLeave,DialogOptionSelected,$option == beaconLeave,DismissDialog,,,\n" +
                ",,,,,,";

        List<RawRule> rawRules = RulesParser.parseCsv(new StringReader(multiLineRule));

        for(RawRule rawRule : rawRules){
            System.out.println(rawRule);
        }
    }

    @Test
    public void TestParsingORs(){
        String multiLine =
                "id,trigger,conditions,script,text,options,notes\n"+
                "# player distress call response - buying supplies and fuel,,,,,,\n" +
                "dcall_normalStart,BeginFleetEncounter,\"!$isHostile\n" +
                "$distressResponse score:1000\n" +
                "!$distressNoHail\",\"AddText \"\"You're being hailed.\"\" $faction.baseColor\",,,\n" +
                "dcall_openCommsInhospitable,OpenCommLink,\"!$isHostile\n" +
                "RepIsAtBest $faction.id INHOSPITABLE\n" +
                "$entity.distressResponse score:1000\",\"DistressResponse unrespond\n" +
                "$entity.ignorePlayerCommRequests = true\n" +
                "FireAll PopulateOptions\",\"\"\"Oh, it's you. I've heard about you. You're on your own.\"\"\n" +
                "OR\n" +
                "\"\"You?\"\" $HeOrShe laughs without mirth then cuts the comm sharply. \",,\n" +
                "dcall_openComms,OpenCommLink,\"RepIsAtWorst $faction.id SUSPICIOUS\n" +
                "!$distressTalkedToPlayerBefore\n" +
                "$entity.distressResponse score:1000\",\"$distressTalkedToPlayerBefore = true\n" +
                "$cutCommLinkPolite = true 0\n" +
                "$entity.distressNoHail = true\n" +
                "DistressResponse init\n" +
                "FireBest DCallShowAidOptions\",\"\"\"Must be a relief we showed instead of some pirates, eh?\n" +
                "\n" +
                "Now, what was it you needed?\"\"\",,\n" +
                "dcall_openCommsRepeat,OpenCommLink,\"RepIsAtWorst $faction.id SUSPICIOUS\n" +
                "$entity.distressResponse score:1000\",\"$entity.distressNoHail = true\n" +
                "$cutCommLinkPolite = true 0\n" +
                "DistressResponse init\n" +
                "FireBest DCallShowAidOptionsRepeat\",\"\"\"Need some help after all, do you?\"\"\",,\n" +
                "dcall_aidOptions,DCallShowAidOptions,,,,\"dcallHelp:\"\"We'll be grateful for anything you can spare.\"\"\n" +
                "dcallMistake:\"\"It was a mistake - we don't actually need help.\"\"\n" +
                "dcallNeverMind:\"\"We didn't issue a distress call.\"\"\",\n" +
                "dcall_aidOptionsRepeat,DCallShowAidOptionsRepeat,,,,\"dcallHelp:\"\"We'll be grateful for anything you can spare.\"\"\n" +
                "cutCommLinkPolite:Cut the comm link\",\n" +
                "dcall_neverMindSel,DialogOptionSelected,$option == dcallNeverMind,\"DistressResponse neverMind\n" +
                "FireAll PopulateOptions\",\"\"\"Is that so? Well, I guess we'll hang around here for a bit and wait for who did.\"\"\n" +
                "\n" +
                "You get the feeling $heOrShe has some doubts about what you've said.\",,\n" +
                "dcall_mistakeSel,DialogOptionSelected,$option == dcallMistake,\"DistressResponse neverMind\n" +
                "DistressResponse unrespond\n" +
                "FireAll PopulateOptions\",\"\"\"Well, at least you owned up to it. Still, a distress call is not something to send out lightly.\"\"\n" +
                "OR\n" +
                "$HeOrShe frowns, and says \"\"Careful, you don't want to earn yourself ... a reputation. It's dangerous out here.\"\"\",,\n" +
                "dcall_helpSelNoNeed,DialogOptionSelected,\"$option == dcallHelp\n" +
                "!DistressResponse playerNeedsHelp score:1000\",\"DistressResponse didNotNeedHelp\n" +
                "$entity.ignorePlayerCommRequests = true\n" +
                "EndConversation NO_CONTINUE\",\"\"\"But... my scans show you've got plenty enough fuel and supplies. I don't appreciate my time being wasted like this.\"\"\n" +
                "\n" +
                "The comm link is cut before you have a chance to respond.\",,\n" +
                "dcall_helpSelScam,DialogOptionSelected,\"$option == dcallHelp\n" +
                "DistressResponse isCargoPodsScam score:200\",\"DistressResponse cargoPodsScam\n" +
                "$entity.ignorePlayerCommRequests = true\n" +
                "EndConversation NO_CONTINUE\",\"\"\"I can see the cargo pods you've dumped overboard, you know.  I'm not blind. And my sensors indicate the presence of fuel or supplies in those.\n" +
                "\n" +
                "I can understand trying to scam a fellow spacer. It's the sheer incompetence of the attempt that's so insulting.\"\"\n" +
                "\n" +
                "The comm link is cut before you have a chance to respond.\",,\n" +
                "dcall_helpSel,DialogOptionSelected,\"$option == dcallHelp\n" +
                "$distressUsesLastCycle <= 2\",\"DistressResponse acceptHelp\n" +
                "FireBest DCallPaymentOptions\",\"\"\"Happy to help a stranded spacer. Do try not to make a habit of getting in trouble, though.\"\"\",,\n" +
                "dcall_helpSel1,DialogOptionSelected,\"$option == dcallHelp\n" +
                "$distressUsesLastCycle > 2\n" +
                "$distressUsesLastCycle <= 4\",\"DistressResponse acceptHelp\n" +
                "FireBest DCallPaymentOptions\",\"\"\"You're getting a bit of a reputation for getting yourself in trouble, you know. Try to be more careful in the future.\"\"\",,\n" +
                "dcall_helpSel2,DialogOptionSelected,\"$option == dcallHelp\n" +
                "$distressUsesLastCycle > 4\",\"DistressResponse acceptHelp\n" +
                "FireBest DCallPaymentOptions\",\"\"\"I'll help you for the sake of your crew. One can't help but think you're being dangerously irresponsible, if you keep needing to be bailed out like this.\"\"\",,\n" +
                "dcall_helpSel4,DialogOptionSelected,\"$option == dcallHelp\n" +
                "$distressUsesLastCycle <= 4\n" +
                "!$distressHelpAdequate score:100\",\"DistressResponse acceptHelp\n" +
                "FireBest DCallPaymentOptions\",\"\"\"Take this - I don't think it's enough, but it's all we have to spare.\"\"\",,\n" +
                "dcall_helpSel5,DialogOptionSelected,\"$option == dcallHelp\n" +
                "$distressUsesLastCycle > 4\n" +
                "!$distressHelpAdequate score:100\",\"DistressResponse acceptHelp\n" +
                "FireBest DCallPaymentOptions\",\"\"\"What we can spare doesn't look like it'll be enough, but you should be thankful to get anything at all considering the number of times you've had to call for help recently.\"\"\",,\n" +
                "dcall_paymentOptions,DCallPaymentOptions,,\"AddText \"\"You have $player.creditsStrC available.\"\"\n" +
                "SetTextHighlights $player.creditsStrC\n" +
                "FireBest DCallPaymentOptionsUpdate\",,\"0:dcallPay:Offer to pay [$distressPaymentC]\n" +
                "1:dcallThank:Thank $himOrHer profusely, but offer no payment\",\n" +
                "dcall_paymentOptionsUpdate,DCallPaymentOptionsUpdate,!$distressCanAfford,\"SetEnabled dcallPay false\n" +
                "SetTooltip dcallPay \"\"Not enough credits to offer to pay a reasonable, non-insulting amount.\"\"\",,,\n" +
                "dcall_paySel,DialogOptionSelected,$option == dcallPay,\"DistressResponse pay\n" +
                "FireAll PopulateOptions\",\"\"\"Thanks, much appreciated. That'll go a long way to offset our costs.\"\"\",,\n" +
                "dcall_thankSel,DialogOptionSelected,$option == dcallThank,\"DistressResponse thank\n" +
                "FireAll PopulateOptions\",\"\"\"Yeah, if you could afford to pay your own way, you probably wouldn't be in this situation in the first place, would you?\"\"\",,";

        List<RawRule> rawRules = RulesParser.parseCsv(new StringReader(multiLine));

        for(RawRule rawRule : rawRules){
            System.out.println(rawRule);
        }
    }

}
