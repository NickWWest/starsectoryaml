package data.scripts.rules;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Rule {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String id;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String trigger;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String conditions;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String script;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String test;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String options;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String notes;

    public Rule(){

    }

    public Rule(RawRule rawRule){
        this.id = rawRule.id;
        this.trigger = rawRule.trigger;
        this.conditions = rawRule.conditions;
        this.script = rawRule.script;
        this.test = rawRule.test;
        this.options = rawRule.options;
        this.notes = rawRule.notes;
    }

    @Override
    public String toString() {
        return "Rule{" +
                "id='" + id + '\'' +
                ", trigger='" + trigger + '\'' +
                ", conditions='" + conditions + '\'' +
                ", script='" + script + '\'' +
                ", test='" + test + '\'' +
                ", options='" + options + '\'' +
                ", notes='" + notes + '\'' +
                '}';
    }
}
