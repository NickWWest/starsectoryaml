# StarSectorYaml

Converts between a CSV based format of Rules and a YAML format of rules.

YAML for those not familiar:
* http://yaml.org/start.html
* https://en.wikipedia.org/wiki/YAML 


##Example
The CSV:
```csv
id,trigger,conditions,script,text,options,notes
# default entity interaction,,,,,,
defaultOpenDialog,OpenInteractionDialog,,"ShowDefaultVisual
PrintDescription 3
SetShortcut defaultLeave ""ESCAPE""",,defaultLeave:Leave,
defaultLeave,DialogOptionSelected,$option == defaultLeave,DismissDialog,,,
```


Turns into:

```yaml
  comment: default entity interaction
  rules:
   -
    id: defaultOpenDialog
    trigger: OpenInteractionDialog
    script: |-
      ShowDefaultVisual
      PrintDescription 3
      SetShortcut defaultLeave "ESCAPE"
    options: defaultLeave:Leave
   -
    id: defaultLeave
    trigger: DialogOptionSelected
    conditions: $option == defaultLeave
    script: DismissDialog
```


##Usage
Args to convert the core rules.csv into rules.yaml
`--action ToYaml --csv "D:\Games\Starsector8.1a\starsector-core\data\campaign\rules.csv" --yaml "D:\dev\src\personal\StarsectorYaml\rules.yaml"`

Args to convert rules.yaml into rules.csv
`--action ToCsv --csv "D:\Games\Starsector8.1a\starsector-core\data\campaign\rules.csv" --yaml "D:\dev\src\personal\StarsectorYaml\rules.yaml"` 