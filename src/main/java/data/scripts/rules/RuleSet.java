package data.scripts.rules;

import java.util.ArrayList;
import java.util.List;

public class RuleSet {
    public String comment;
    public List<Rule> rules = new ArrayList<>();

    @Override
    public String toString() {
        return "RuleSet{" +
                "comment='" + comment + '\'' +
                ", RuleCount=" + rules.size() +
                '}';
    }
}
