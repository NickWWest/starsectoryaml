package data.scripts.rules;

import com.univocity.parsers.annotations.Parsed;
import com.univocity.parsers.annotations.Replace;
import com.univocity.parsers.annotations.Trim;

/**
 * Used in parsing rule.csv lines into an object, a simple data bucket
 */
public class RawRule {

    @Parsed(index = 0)
    @Trim
    @Replace(expression = "\r", replacement = "")
    public String id;

    @Parsed(index = 1)
    @Trim
    @Replace(expression = "\r", replacement = "")
    public String trigger;

    @Parsed(index = 2)
    @Trim
    @Replace(expression = "\r", replacement = "")
    public String conditions;

    @Parsed(index = 3)
    @Trim
    @Replace(expression = "\r", replacement = "")
    public String script;

    @Parsed(index = 4)
    @Trim
    @Replace(expression = "\r", replacement = "")
    public String test;

    @Parsed(index = 5)
    @Trim
    @Replace(expression = "\r", replacement = "")
    public String options;

    @Parsed(index = 6)
    @Trim
    @Replace(expression = "\r", replacement = "")
    public String notes;

    public RawRule(){

    }

    public RawRule(String comment){
        id = "# "+comment;
    }

    public RawRule(Rule r){
        this.id = r.id;
        this.trigger = r.trigger;
        this.conditions = r.conditions;
        this.script = r.script;
        this.test = r.test;
        this.options = r.options;
        this.notes = r.notes;
    }

    @Override
    public String toString() {
        return "RawRule{" +
                "id='" + id + '\'' +
                ", trigger='" + trigger + '\'' +
                ", conditions='" + conditions + '\'' +
                ", script='" + script + '\'' +
                ", test='" + test + '\'' +
                ", options='" + options + '\'' +
                ", notes='" + notes + '\'' +
                '}';
    }

    public boolean isComment(){
        return id.startsWith("#") && trigger == null;
    }

    public String getComment(){
        return id.substring(1, id.length()).trim();
    }

    public boolean isEmpty(){
        return id == null || id.length() == 0;
    }
}
